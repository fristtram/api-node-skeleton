# Getting Started with Create NodeJS

## Prerequisites

This project requires NodeJS (version v18.16.0 or later) and YARN.

``` bash
# install dependencies
$ npm -v && node -v
9.5.1
v18.16.0
```

## Installation

``` bash
# clone the project
$ git clone https://gitlab.com/fristtram/api-node-skeleton.git
$ cd api-node-skeleton

# install dependencies
npm install

# serve with hot reload at localhost:3333
npm run dev

# build for production with minification
npm run build

# Prisma
npx prisma init --datasource-provider SQLite

# MIGRATION PRISMA
npx prisma migrate dev
npx prisma migrate reset

# Visualizar Banco de dados
npx prisma studio

# Acessar o banco de dados
npm i @prisma/client

# Config Produção
npm ci && npm run build && npx prisma migrate deploy

```