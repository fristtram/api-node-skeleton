import { randomUUID } from 'node:crypto'
import { extname, resolve } from 'node:path'
import { createWriteStream } from 'node:fs'
import { FastifyInstance } from 'fastify'
import { pipeline } from 'node:stream'
import { promisify } from 'node:util'
import { prisma } from '../lib/prisma'
import { z } from 'zod'

const pump = promisify(pipeline)

export async function updateRoutes(app: FastifyInstance) {
  app.post('/upload/user/:user_id', async (request, reply) => {
    const upload = await request.file({
      limits: {
        fileSize: 5_242_880, // 5Mb
      },
    })

    if (!upload) {
      return reply.status(400).send()
    }

    const mimeTypeRegex = /^(image|video)\/[a-zA-Z]+/
    const isValidFileFormat = mimeTypeRegex.test(upload.mimetype)

    if (!isValidFileFormat) {
      return reply.status(400).send()
    }

    const fileId = randomUUID()
    const extension = extname(upload.filename)

    const fileName = fileId.concat(extension)

    const writeStream = createWriteStream(
      resolve(__dirname, '../../uploads/user', fileName),
    )

    // Amazon S3, Goggle GCS, Cloudflare R2
    await pump(upload.file, writeStream)

    const fullUrl = request.protocol.concat('://').concat(request.hostname)
    const fileUrl = new URL(`/uploads/user/${fileName}`, fullUrl).toString()

    const paramsSchema = z.object({
      user_id: z.string().uuid(),
    })
    const { user_id } = paramsSchema.parse(request.params)

    const user = await prisma.user.findUniqueOrThrow({
      where: {
        id: user_id,
      },
    })
    if (user.name === null || user.name === undefined) {
      return reply.status(401).send()
    }

    await prisma.user.update({
      where: {
        id: user_id,
      },
      data: {
        avatarUrl: fileUrl,
      },
    })

    return { fileUrl }
  })
}
