import { FastifyInstance } from 'fastify'
import { prisma } from '../lib/prisma'
import { z } from 'zod'

export async function permissaoRoutes(app: FastifyInstance) {
  app.get('/permissao', async (request) => {
    const permissaos = await prisma.permissao.findMany({
      orderBy: {
        name: 'asc',
      },
    })

    return permissaos.map((permissao) => {
      return {
        id: permissao.id,
        name: permissao.name,
      }
    })
  })

  app.post('/permissao', async (request) => {
    const bodySchema = z.object({
      name: z.string(),
    })

    const { name } = bodySchema.parse(request.body)

    const permissao = await prisma.permissao.create({
      data: {
        name,
      },
    })

    return permissao
  })

  app.put('/permissao/:id', async (request, reply) => {
    const paramsSchema = z.object({
      id: z.string().uuid(),
    })

    const { id } = paramsSchema.parse(request.params)

    const bodySchema = z.object({
      name: z.string(),
    })

    const { name } = bodySchema.parse(request.body)

    let permissao = await prisma.permissao.findUniqueOrThrow({
      where: {
        id,
      },
    })

    permissao = await prisma.permissao.update({
      where: {
        id,
      },
      data: {
        name,
      },
    })

    return permissao
  })

  app.delete('/permissao/:id', async (request, reply) => {
    const paramsSchema = z.object({
      id: z.string().uuid(),
    })

    const { id } = paramsSchema.parse(request.params)

    await prisma.permissao.delete({
      where: {
        id,
      },
    })
  })
}
