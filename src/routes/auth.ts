import { FastifyInstance } from 'fastify'
import { z } from 'zod'
import { prisma } from '../lib/prisma'
import * as bcrypt from 'bcrypt'

export async function authRoutes(app: FastifyInstance) {
  app.post('/auth', async (request, reply) => {
    const bodySchema = z.object({
      email: z.string().email(),
      password: z.string(),
    })

    const { email, password } = bodySchema.parse(request.body)

    const users = await prisma.user.findFirstOrThrow({
      include: {
        permissao: true,
      },
      where: {
        email,
      },
    })

    if (!users) {
      return reply.status(401).send('Usuário não existe')
    }

    const checkPassword = bcrypt.compareSync(password, users.password)
    if (!checkPassword) {
      return reply.status(401).send('E-mail ou password está incorreto!')
    }

    const name = users.name
    const permissao = users.permissao.name

    const token = app.jwt.sign(
      {
        id: users.id,
        name: users.name,
        email: users.email,
        avatarUrl: users.avatarUrl,
        permissaoId: users.permissaoId,
        status: users.status,
      },
      {
        sub: users.id,
        expiresIn: '1 days',
      },
    )

    return {
      name,
      email,
      permissao,
      token,
    }
  })
}
