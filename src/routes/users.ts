import { FastifyInstance } from 'fastify'
import { prisma } from '../lib/prisma'
import { z } from 'zod'
import * as bcrypt from 'bcrypt'

export async function usersRoutes(app: FastifyInstance) {
  app.get('/users', async (request) => {
    await request.jwtVerify()

    const users = await prisma.user.findMany({
      orderBy: {
        name: 'asc',
      },
    })

    return users.map((user) => {
      return {
        id: user.id,
        name: user.name,
        email: user.email,
        avatarUrl: user.avatarUrl,
        status: user.status,
        permissaoId: user.permissaoId,
        createdAt: user.createdAt,
      }
    })
  })

  app.get('/user/:id', async (request, reply) => {
    app.addHook('preHandler', async (request) => {
      await request.jwtVerify()
    })
    const paramsSchema = z.object({
      id: z.string().uuid(),
    })

    const { id } = paramsSchema.parse(request.params)

    const user = await prisma.user.findUniqueOrThrow({
      where: {
        id,
      },
    })

    return user
  })

  app.post('/users', async (request) => {
    const salt = bcrypt.genSaltSync(10)
    const bodySchema = z.object({
      name: z.string(),
      email: z.string().email(),
      password: z.string(),
      permissaoId: z.string(),
    })

    const { name, email, password, permissaoId } = bodySchema.parse(
      request.body,
    )

    const user = await prisma.user.create({
      data: {
        name,
        email,
        password: bcrypt.hashSync(password, salt),
        permissaoId,
      },
    })

    return user
  })

  app.post('/create-new-user', async (request) => {
    const salt = bcrypt.genSaltSync(10)

    const bodySchema = z.object({
      name: z.string(),
      email: z.string().email(),
      permissaoId: z.string(),
    })

    const { name, email, permissaoId } = bodySchema.parse(request.body)

    const chars = '23456789abcdefghijkmnpqrstuvwxyzABCDEFGHIJKLMNPQRSTUVWXYZ'
    const passwordLength = 5
    let password = ''

    for (let i = 0; i <= passwordLength; i++) {
      const randomNumber = Math.floor(Math.random() * chars.length)
      password += chars.substring(randomNumber, randomNumber + 1)
    }

    console.log(password)

    const user = await prisma.user.create({
      data: {
        name,
        email,
        password: bcrypt.hashSync(password, salt),
        permissaoId,
      },
    })

    return user
  })

  app.put('/users/:id', async (request, reply) => {
    const paramsSchema = z.object({
      id: z.string().uuid(),
    })

    const { id } = paramsSchema.parse(request.params)

    const bodySchema = z.object({
      name: z.string(),
      email: z.string(),
      avatarUrl: z.string(),
      status: z.string(),
      permissaoId: z.string(),
    })

    const { name, email, avatarUrl, status, permissaoId } = bodySchema.parse(
      request.body,
    )

    let user = await prisma.user.findUniqueOrThrow({
      where: {
        id,
      },
    })

    if (user.name === null || user.name === undefined) {
      return reply.status(401).send()
    }

    user = await prisma.user.update({
      where: {
        id,
      },
      data: {
        name,
        email,
        avatarUrl,
        status,
        permissaoId,
      },
    })

    return user
  })

  app.delete('/user/:id', async (request, reply) => {
    const paramsSchema = z.object({
      id: z.string().uuid(),
    })

    const { id } = paramsSchema.parse(request.params)

    const user = await prisma.user.findUniqueOrThrow({
      where: {
        id,
      },
    })

    if (user.name === null || user.name === undefined) {
      return reply.status(401).send()
    }

    await prisma.user.delete({
      where: {
        id,
      },
    })
  })
}
