import 'dotenv/config'

import fastify from 'fastify'
import { permissaoRoutes } from './routes/permissao'
import { usersRoutes } from './routes/users'
import cors from '@fastify/cors'
import jwt from '@fastify/jwt'
import multipart from '@fastify/multipart'
import { authRoutes } from './routes/auth'
import { updateRoutes } from './routes/upload'
import { resolve } from 'node:path'

const app = fastify()

app.register(multipart)
app.register(require('@fastify/static'), {
  root: resolve(__dirname, '../uploads'),
  prefix: '/uploads',
})

app.register(cors, {
  origin: true, // default para qualquer frontend acessar
  // origin: ['http://localhost:3000', 'https://localhost:3001'],
})

app.register(jwt, {
  secret: 'eleicaokiriawebpolonsoftware', // private
})

app.register(authRoutes)
app.register(updateRoutes)
app.register(permissaoRoutes)
app.register(usersRoutes)

app
  .listen({
    port: process.env.PORT ? Number(process.env.PORT) : 3333,
    host: '0.0.0.0',
  })
  .then(() => {
    console.log('HTTP server listening on http://localhost:3333')
  })
